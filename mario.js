let contador = 0;
$(function () {
    $(document).keyup(function (tecla) {
        // derecha 39
        // izquierda 37
        // z 90

        // Con la tecla derecha hacemos que las dos imagenes se muevan 100 px a la derecha
        if (tecla.keyCode == 39 && contador < 4) {
            $(".mario, .estrella").animate({ left: '+=100px' });
            contador++;

        }
        // Con la tecla izquierda hacemos que las dos imagenes se muevan 100 px a la izquierda
        if (tecla.keyCode == 37 && contador > 0) {
            $(".mario, .estrella").animate({ left: '-=100px' });

            contador--;
        }

        // Con la tecla Z hacemos que las estrella suba y que luego baje
        if (tecla.keyCode == 90) {
            $(".estrella").animate({ top: '-=550px' });
            $(".estrella").animate({ top: '+=550px' });
        }


    });
});
